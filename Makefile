SHELL := /bin/sh

# Defaults used for both debug AND release
CC := clang
# CPPFLAGS :=
CFLAGS := -pthread

# Linker config
# LDFLAGS :=
# LDLIBS :=

BIN := skeleton

SRCDIR := src
SRCS := $(wildcard $(SRCDIR)/*.c)
OBJDIR = obj
OBJS = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))
BINDIR = bin

DEPS = $(OBJS:.o=.d)
DEPFLAGS = -MMD -MF $(@:.o=.d)

.PHONY: all
all: debug

.PHONY: debug
debug: CFLAGS += -Wall -Wextra -ggdb3 -Og
debug: TARGET_LOCK := .debug
debug: prebuild $(BINDIR)/$(BIN) | .$@

.PHONY: release
release: CPPFLAGS += -DNDEBUG
release: CFLAGS += -Wall -O2
release: TARGET_LOCK := .release
release: prebuild $(BINDIR)/$(BIN) | .$@

-include $(DEPS)

.PHONY: prebuild
prebuild:
	@if [ ! -f '$(OBJDIR)/$(TARGET_LOCK)' ]; then \
		$(RM) -r $(OBJDIR) $(BINDIR); \
		mkdir $(OBJDIR); \
		touch $(OBJDIR)/$(TARGET_LOCK); \
		echo "Changing target to: $(TARGET_LOCK)"; \
	else \
		echo "Already using target: $(TARGET_LOCK)"; \
	fi
		

$(BINDIR)/$(BIN): $(OBJS) | $(BINDIR)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@ $(DEPFLAGS)

$(BINDIR) $(OBJDIR) $(SRCDIR):
	mkdir -p $@

.PHONY: run
run: $(BINDIR)/$(BIN)
	./$<

.PHONY: clean
clean:
	$(RM) -r $(BINDIR) $(OBJDIR)
